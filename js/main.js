var URLS = [
  '../../img/naah.png',
  '../../img/hmm.png',
  '../../img/sigh.png',
  '../../img/shake_.gif',
  '../../img/dance_.gif',
  '../../img/full.png',
  '../../img/whaat_.gif',
  '../../img/dance2_.gif',
  '../../img/money_.gif',
  '../../img/alarm_.gif',
  '../../img/love.png',
  '../../img/walk.gif',
];

var COLORS = [
  'rgb(165, 198, 239)',
  '#D7EDAF',
  '#F2CC7B',
  '#FC7E7E',
  '#bad5dd',
  '#A2D8C0'
];

function pick(array) {
  var idx = Math.floor(Math.random() * array.length);
  return array[idx];
}

function pickImage() {
  return pick(URLS);
}

function pickBackgroundColor(image) {
  return image.indexOf('_.gif') !== -1 ? 'white' : pick(COLORS);
}

var image = pickImage();
var color = pickBackgroundColor(image);

document.getElementById('img').src = image;
document.getElementById('body').style = "background-color: " + color;
